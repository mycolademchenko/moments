import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home'
import Create from '@/views/Create'
import View from '@/views/View'
import ViewMoments from '@/views/ViewMoments'
import auth from '@/main'
import Auth from '@/views/Auth'

Vue.use(Router)

export const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: { requireAuth: true } // TODO make common
    },
    {
      path: '/Create/:videoID',
      name: 'Create',
      component: Create,
      props: true,
      meta: { requireAuth: true } // TODO make common
    },
    {
      path: '/ViewMoments/:videoId',
      name: 'ViewMoments',
      component: ViewMoments,
      props: true
    },
    {
      path: '/view/:id',
      name: 'Link',
      component: View
    },
    {
      path: '/auth',
      name: 'auth',
      component: Auth,
      meta: { guestOnly: true }
    }
  ]
})

router.beforeEach((to, from, next) => {
  let currentUser = auth.user()
  let requireAuth = to.matched.some(record => record.meta.requireAuth)
  let guestOnly = to.matched.some(record => record.meta.guestOnly)

  if (requireAuth && !currentUser) next('/auth')
  else if (guestOnly && currentUser) next('/')
  else next()
})
