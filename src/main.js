import Vue from 'vue'
import App from './App'
import VueYoutube from 'vue-youtube'
import VueDraggableResizable from 'vue-draggable-resizable'
import BootstrapVue from 'bootstrap-vue'
import VueKonva from 'vue-konva'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faShare, faPlay, faStop, faVideo } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {router} from '@/router'
import {store} from '@/store'
import firebaseui from 'firebaseui'
import firebase from 'firebase'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

library.add(faShare, faPlay, faStop, faVideo)

Vue.component('font-awesome-icon', FontAwesomeIcon)

// import vueinterval from 'vue-interval/dist/VueInterval.common'
// import router from './router'

Vue.use(VueYoutube)
Vue.use(BootstrapVue)
Vue.use(VueKonva)

Vue.component('vue-draggable-resizable', VueDraggableResizable)

Vue.config.productionTip = false

const config = {
  apiKey: 'AIzaSyA4UY8neTZn3342X7vY9o0j0PFge3fC5a4',
  authDomain: 'moments-2df9b.firebaseapp.com',
  databaseURL: 'https://moments-2df9b.firebaseio.com',
  projectId: 'moments-2df9b',
  storageBucket: 'moments-2df9b.appspot.com',
  messagingSenderId: '296911754202'
}

firebase.initializeApp(config)

export const db = firebase.firestore() // TODO

const auth = {
  context: null,
  uiConfig: null,
  ui: null,

  init (context) {
    this.context = context

    this.uiConfig = {
      signInSuccessUrl: '/',
      signInOptions: [
        firebase.auth.GoogleAuthProvider.PROVIDER_ID
      ]
    }
    this.ui = new firebaseui.auth.AuthUI(firebase.auth())

    firebase.auth().onAuthStateChanged((user) => {
      this.context.$store.dispatch('user/setCurrentUser')

      let requireAuth = this.context.$route.matched.some(record => record.meta.requireAuth)
      let guestOnly = this.context.$route.matched.some(record => record.meta.guestOnly)

      if (requireAuth && !user) this.context.$router.push('auth')
      else if (guestOnly && user) this.context.$router.push('/')
    })
  },
  authForm (container) {
    this.ui.start(container, this.uiConfig)
  },
  user () {
    return this.context ? firebase.auth().currentUser : null
  },
  logout () {
    firebase.auth().signOut()
  }
}

export default auth

new Vue({
  router,
  store,
  beforeCreate () {
    auth.init(this)
  },
  created () {
    this.$store.commit('init')
  },
  render: h => h(App)
}).$mount('#app')
