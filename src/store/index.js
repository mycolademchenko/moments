import Vue from 'vue'
import Vuex from 'vuex'

import user from './modules/user'
import { db } from '@/main'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    db,
    moments_: [],
    momentsWhere_: [],
    videos_: [],
    users_: [],
    selectedUser_: null
  },
  getters: {
    db: state => { return state.db },
    moments: state => { return state.moments_ },
    momentsWhere: (state) => (vid) => {
      state.db
        .collection('moment')
        .where('v_id', '==', vid)
        .where('uid', '==', state.user.user.uid)
        .orderBy('date', 'desc')
        .onSnapshot(d => { store.commit('setMomentsWhere', d) })
      return state.momentsWhere_
    },
    videos: state => {
      // Array.prototype.forEach.call(state.moments_, function(v, i, a) {
      //   console.log(v.date, v.dn, v.email, v.end, v.start, v.title, v.uid, v.v_id)
      // })
      state.videos_ = state.moments_.reduce((acc, c) => {
        if (!acc.includes(c.v_id) && c.dn !== state.selectedUser_) acc.push(c.v_id)
        return acc
      }, [])
      return state.videos_
    },
    users: state => {
      state.users_ = state.moments_.reduce((acc, c) => {
        if (!acc.includes(c.dn)) acc.push(c.dn)
        return acc
      }, [])
      return state.users_
    }
  },
  mutations: {
    init: state => {
      state.db = db
      state.db.collection('moment').onSnapshot((d) => { store.commit('setMoments', d) })
    },
    setMoments: (state, QSnapshot) => {
      state.moments_ = []
      QSnapshot.docs.forEach(v => {
        state.moments_ = [...state.moments_, v.data()]
      })
    },
    setMomentsWhere: (state, QSnapshot) => {
      state.momentsWhere_ = []
      QSnapshot.docs.forEach(v => {
        state.momentsWhere_ = [...state.momentsWhere_, v.data()]
      })
    },
    filterByName: (state, dn) => {
      state.selectedUser_ = dn
    }
  },
  actions: {
    create: (ctx, attr) => {
      if (attr.start > attr.end) return 'Start can`t be later than End arr'
      ctx.state.db.collection('moment').add(attr)
      return null
    }
  },
  modules: {
    user
  }
})
